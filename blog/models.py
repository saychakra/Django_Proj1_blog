from django.db import models

class Post(models.Model):
    title = models.CharField(max_length = 140)
    body  = models.TextField()
    date  = models.DateTimeField()
    #now, when ever we need to reference this blog in future for say, the title
    #of the blog posts, obviously, title will be returned as an object of the
    #'Post' class. Thus we need to add a method, to be able to return the string
    #form of the tile used. Hence the following needs to be done

    def __str__(self):
        return self.title
    #what this does, is, it returns the title of the post in the form of a string
    #which obviously for humane purposes are more useful!!
