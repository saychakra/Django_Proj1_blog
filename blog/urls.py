from django.conf.urls import url, include
from django.views.generic import ListView, DetailView
from blog.models import Post

urlpatterns = [
    url(r'^$', ListView.as_view(queryset=Post.objects.all().order_by("-date")[:20],
                                template_name = "blog/blog.html")),
    url(r'^(?P<pk>\d+)$', DetailView.as_view(model = Post, template_name = 'blog/first_post.html'))

        ]

##1st url:

#what is being done here is that the url is setting a path to blog.html from
#where we will be querying "all" the objects of the "post" in a "descending order
#of date- hence the '-' sign before date, and we will be querying only the
#first 20 posts from the blog hence the [:20]. thereby the template is defined
#with the proper path, ALWAYS starting from the templates directory (remember)
#also ListView is given because we need to query out a list of the blogs that
#that were created in a list format
    

##2nd url:
##The second url defines the blogs which are being referenced to
##like 127.0.0.1:8000/blog has been referenced and now we want to say, access
##the first blog post. Essentially the 1st post has a digit '1' and the corresponding
##url is like 127.0.0.1:8000/blog/1
##this one is essentially the primary key attribute of the blog post model created
##the regex in the 2nd url defined the following
## 1. it begins ie ^
## 2. withing the () it says, the following
    ## a. ?P corresponds to a named group (search for it)
    ## b. <pk> says that it is a primary key
    ## c. \d says that it is a digit
    ## d. \d+ says that it is a digit starting with 1 and goes beyond to any subsequent large value.

